#include<stdio.h>
#include<ctype.h>
char lower(char c);
int main()
{
    char c;
    while((c=getchar())!=EOF)
    {
        putchar(lower(c));
    }

    return 0;
}

char lower(char c)
{
    /* 需要调用isupper()判断是否为大写字母 */
    return (isupper(c))? c+32 : c;
}