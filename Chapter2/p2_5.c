/* 返回str2中的任意字符在str1中第一次出现的位置 */
#include<stdio.h>

int any(char s1[], char s2[]);    /* 可参考标准库strpbrk进行实现 */
int main()
{
    char* str1 = "astrcsrwz";
    char* str2 = "cw";

    printf("%d",any(str1, str2));
    
    return 0;
}

int any(char s1[], char s2[])
{
    int i, j;
    for(i = 0; s1[i] != '\0'; i++)
        for(j = 0; s2[j] != '\0'; j++)
        {
            if(s2[j] == s1[i])
                return i;
        }
    return -1;
}