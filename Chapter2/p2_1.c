/* 
 * 编写一个程序以确定分别由signed及unsigned限定的char、short、int与long类型变量的取值范围。
 * 采用打印标准头文件中的相应值以及直接计算两种方式实现。
 */
#include<stdio.h>
#include<limits.h>
#include<float.h>

/* 题目要求两种形式打印 */
void func1();
void func2();
int main()
{
    func1();
    func2();
    return 0;
}

void func1()
{
    /* signed type */
    printf("SIGN_CHAR_MAX:%22d\tSIGN_CHAR_MIN:%20d\n",CHAR_MAX, CHAR_MIN); /* 这里标答给的是SCHAR_MAX与SCHAR_MIN是C90/C99给的定义 */
    /* printf("SIGN_CHAR_MAX:%22d\tSIGN_CHAR_MIN:%20d\n",SCHAR_MAX, SCHAR_MIN); */
    printf("SIGN_INT_MAX:%23d\tSIGN_INT_MIN:%21d\n",INT_MAX, INT_MIN);
    printf("SIGN_SHORT_MAX:%21d\tSIGN_SHORT_MIN:%19d\n",SHRT_MAX, SHRT_MIN);
    printf("SIGN_LONG_MAX:%22ld\tSIGN_LONG_MIN:%20ld\n",LONG_MAX, LONG_MIN);

    /* unsigned type */
    printf("UNSIGN_CHAR_MAX:%20u\tUNSIGN_CHAR_MIN:%18u\n",UCHAR_MAX, 0);
    printf("UNSIGN_INT_MAX:%21u\tUNSIGN_INT_MIN:%19u\n",UINT_MAX, 0);
    printf("UNSIGN_SHORT_MAX:%19u\tUNSIGN_SHORT_MIN:%17u\n",USHRT_MAX, 0);
    printf("UNSIGN_LONG_MAX:%20lu\tUNSIGN_LONG_MIN:%18u\n",ULONG_MAX, 0);
}

void func2()
{
    /* 通过计算求出范围，相对较为困难, 标答给的是位运算方法 */
    printf("signed char min = %d\n",-(char)((unsigned char) ~0 >> 1));
    printf("signed char max = %d\n",(char)((unsigned char) ~0 >> 1));
    printf("signed short min = %d\n",-(short)(unsigned short) ~0 >> 1);
    printf("signed short max = %d\n",(short)(unsigned short) ~0 >> 1);
    printf("signed int min = %d\n",-(int)((unsigned int) ~0 >> 1));
    printf("signed int max = %d\n", (int)((unsigned int) ~0 >> 1));
    printf("signed long min = %ld\n",-(long)((unsigned long) ~0 >> 1));
    printf("signed long max = %ld\n", (long)((unsigned long) ~0 >> 1));

    /* unsigned types */
    printf("unsigned char max = %u\n",(unsigned char)~0);
    printf("unsigned short max = %u\n",(unsigned short)~0);
    printf("unsigned int max = %u\n",(unsigned int)~0);
    printf("unsigned long max = %lu\n",(unsigned long)~0);
}