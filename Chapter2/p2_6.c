#include<stdio.h>
#include<string.h>
#include<assert.h>
#define MAX 100     /* 这里限制了输入字符数组的大小 */

char* setbits(char x[], int p, int n, char y[]);
int main()
{
    char bin_n1[MAX];
    char bin_n2[MAX];
    unsigned int p, n;

    char* bin_n3 = 0;

    /* 输入两个二进制数 */
    scanf("%s",bin_n1);
    scanf("%s",bin_n2);
    /* 输入p，n的值 */
    scanf("%ud", &p);
    scanf("%ud", &n);
    
    /* 测试时不要输入字符串常量，字符串常量不允许更改 */
    bin_n3 = setbits(bin_n1, p, n, bin_n2);
    printf("%s", bin_n3);
    return 0;
}

/* x, y为两个二进制字符数 */
char* setbits(char x[], int p, int n, char y[])
{
    /* */
    int x_length = strlen(x);
    int y_length = strlen(y);
    int i;

    /* 防止给出的p和n越bin_n1和bin_n2的界 */
    assert(!(p+n > x_length || n > y_length));

    for(i = 0; i < n; i++)
    {
        x[p+i] = y[y_length-1-i];
    }
    return x;
}