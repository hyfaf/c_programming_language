#include<stdio.h>
#include<string.h>
#include<assert.h>
#define MAX 100

char* invert(char x[], int p, int n);
int main()
{
    char bin_n[MAX];
    unsigned int p, n;

    scanf("%s", bin_n);
    scanf("%ud", &p);
    scanf("%ud", &n);

    char* s = invert(bin_n, p, n);

    printf("%s", s);
    return 0;
}

char* invert(char x[], int p, int n)
{
    assert(p>=0 && p < strlen(x));
    int i;
    
    for(i = 0; i < n; i++)
    {
        if(x[p+i] == '0')
            x[p+i] = '1';
        else
            x[p+i] = '0';
    }

    return x;
}
