/*
 * 该程序有两个转换函数escape和inverse_escape
 * escape是将输入的字符串t中的所有不可见字符转换成可见字符并拷贝到字符串s中
 * inverse_escape是将输入字符串t中的可见字符转换成不可见字符并拷贝到字符串s中，是escape的反操作
 */
#include<stdio.h>
#define MAX 1000

void escape(char s[], const char t[]);
void inverse_escape(char s[], const char t[]);

int main()
{
    char s[MAX];        /* 存储字符串escape后的结果 */
    char s2[MAX];       /* 存储字符串inverse_escape后的结果 */
    char t[MAX];        /* 存储输入的测试字符串 */
    char c;
    int i = 0;

    while((c=getchar())!=EOF)
    {
        t[i++] = c;
    }
    t[i] = '\0';


    printf("\n--------------------\n");
    escape(s,t);
    printf("\n%s", s);
    printf("\n--------------------\n");
    inverse_escape(s2,s);
    printf("\n%s", s2);
    

    return 0;
}

void escape(char s[], const char t[])
{
    int i,j;

    for(i = j = 0;t[i] != '\0'; i++)
    {
        switch (t[i])
        {
            case '\n':
            {
                s[j++] = '\\';
                s[j++] = 'n';
                break;
            }
            case '\t':
            {
                s[j++] = '\\';
                s[j++] = 't';
                break;
            }
            case '\b':
            {
                s[j++] = '\\';
                s[j++] = 'b';
                break;
            }
            case '\f':
            {
                s[j++] = '\\';
                s[j++] = 'f';
                break;
            }
            default:
            {
                s[j++] = t[i];
                break;
            }
        }
    }
    s[j] = '\0';
}

/* 反向替换函数 */
void inverse_escape(char s[], const char t[])
{
    int i,j;

    for(i = j = 0;t[i] != '\0'; i++)
    {
        switch (t[i])
        {
            case '\\':
            {
                if(t[i+1] == 'n')   // 不在if中使用++的原因在于每判断一次都会自增破坏了逻辑
                {
                    s[j++] = '\n';
                    i++;
                }
                else if(t[i+1] == 't')
                {
                    s[j++] = '\t';
                    i++;
                }
                else if(t[i+1] == 'b')
                {
                    s[j++] = '\b';
                    i++;
                }
                else if(t[i+1] == 'f')
                {
                    s[j++] = '\f';
                    i++;
                }
                else
                    s[j++] = t[i];

                break;
            }
            default:
            {
                s[j++] = t[i];
                break;
            }
        }
    }
    s[j] = '\0';
}