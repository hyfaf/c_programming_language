#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define MAXCHAR 100000      
#define true  1
#define false 0

void copy_and_remove_comment(char commented_code[], char no_commented_code[]);

int main(int argc, char *argv[])
{
    FILE *f = fopen(argv[1],"r");               
    FILE *f2 = fopen("uncomment_code.c", "w");  
    char commented_code[MAXCHAR];
    char no_commented_code[MAXCHAR];            

    fread(commented_code, 1, MAXCHAR, f);       
    copy_and_remove_comment(commented_code, no_commented_code); 
    printf("%s", no_commented_code);            
    fwrite(no_commented_code, 1, strlen(no_commented_code), f2);    

    return 0;
}

void copy_and_remove_comment(char commented_code[], char no_commented_code[])
{
    int signal = false;     
    long nums = strlen(commented_code);
    int i;                  
    int j = 0;              

    

    for(i = 0; i < nums; i++)
    {
        if(commented_code[i]=='/' && commented_code[i+1]=='*')
        {
            signal = true;
        }
        else if(commented_code[i]=='/' && commented_code[i-1]=='*')
        {
            signal = false;
        }
        else if(signal == false)
        {
            no_commented_code[j] = commented_code[i];
            j++;
        }
    }
}
