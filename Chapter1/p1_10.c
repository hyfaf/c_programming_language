#include<stdio.h>
int main()
{
    int c;

    while((c=getchar()) != EOF)
    {
        if(c == '\b')
        {
            /* 只用标准函数库的话，这个退格符好像打印不了 */
            /* 可尝试用非标准函数库中conio.h,中的getch() */
            printf("\\b");
        }
        else if(c == '\t')
        {
            printf("\\t");
        }
        else if(c == '\\')
        {
            printf("\\\\");
        }
        else
        {
            printf("%c", c);
        }
    }
    return 0;
}