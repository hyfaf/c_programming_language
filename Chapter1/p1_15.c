/* 打印一个华氏度转摄氏度的转换表 */
#include<stdio.h>

float Fahr_to_Celsius(float fahr);
int main()
{
    float fahr, celsius;
    int lower, upper, step;

    lower = 0;
    upper = 300;
    step = 20;

    fahr = lower;
    printf("华氏度-摄氏度转换表\n");
    while(fahr <= upper)
    {
        celsius = Fahr_to_Celsius(fahr);
        printf("%3.0f %6.1f\n", fahr, celsius);
        fahr = fahr + step;
    }

    return 0;
}

float Fahr_to_Celsius(float fahr)
{
    return (5.0/9.0) * (fahr-32.0);
}