#include<stdio.h>
int main()
{
    int c;

    while((c=getchar())!=EOF)
    {
        if(c == ' ' || c == '\n' || c == '\t')
        {
            putchar('\n');
            continue;
        }
        putchar(c);
    }

    return 0;
}