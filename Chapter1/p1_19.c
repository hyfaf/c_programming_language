#include<stdio.h>
#include<string.h>
#define MAXLINE 1000

int getline(char s[], int lim);
void reverse(char s[]);

int main()
{
    char line[MAXLINE];     /* 当前的输入行 */

    while(getline(line, MAXLINE) != EOF)   
    {
        reverse(line);
        printf("%s", line);
    }

    return 0;
}

/* getline函数：将一行读入s中并返回其长度 */
int getline(char s[], int lim)
{
    int c, i;

    for(i=0; i<lim-1 && (c=getchar())!=EOF && c != '\n'; ++i)
        s[i] = c;       /* 循环读取每一个字符，遇到换行符或文件结尾跳出 */
    if (c == '\n')      /* 若是换行符则在字符数组末尾加上换行符 */
    {
        s[i] = c;
        ++i;
    }
    s[i] = '\0';
    return i;
}

/* 颠倒传入的字符串 */
void reverse(char s[])
{
    int left = 0;
    int right = strlen(s)-1;

    while(left < right)
    {
        char temp;

        temp = s[left];
        s[left] = s[right];
        s[right] = temp;

        left++;
        right--;
    }
}