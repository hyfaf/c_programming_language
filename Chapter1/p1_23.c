/*
 *  该程序是删除C语言源代码中的所有注释，并将结果代码打印并保存在uncomment_code.c源代码中
 *  程序的些许不足：1.只考虑了C语言注释。 2. 默认输入的源代码是正确的，故意的错误代码用例会导致程序无法运行
 *  3。没有使用动态分配内存，当源代码字符数超过字符数组上限可能无法正常工作。
 */
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define MAXCHAR 100000      /* 字符数组最大储值 */
#define true  1
#define false 0

void copy_and_remove_comment(char commented_code[], char no_commented_code[]);

int main(int argc, char *argv[])
{
    FILE *f = fopen(argv[1],"r");               /* 打开带注释的C源代码文件*/
    FILE *f2 = fopen("uncomment_code.c", "w");  /* 打开一个空的C源代码文件 */
    char commented_code[MAXCHAR];
    char no_commented_code[MAXCHAR];            /* 用两个大小固定的字符数组储存修改前和修改后的源代码 */

    fread(commented_code, 1, MAXCHAR, f);       /* 将带注释的C源代码数据写入commented_code字符数组 */
    copy_and_remove_comment(commented_code, no_commented_code); /* 去除C源代码中的注释行，并将数据拷贝到no_commented_code字符数组 */
    printf("%s", no_commented_code);            /* 终端打印去除注释的源代码 */
    fwrite(no_commented_code, 1, strlen(no_commented_code), f2);    /* 将去除注释的源代码数据写入uncomment_code.c文件 */

    return 0;
}

void copy_and_remove_comment(char commented_code[], char no_commented_code[])
{
    int signal = false;     /* 用于判断在读部分是否为注释 */
    long nums = strlen(commented_code);
    int i;                  /* 控制comment_code的索引 */
    int j = 0;              /* 控制no_comment_code的索引 */

    /* 
     * 此处for循环的整体逻辑就是：遇到注释的左端字符串则将记号记为true从该字符串开始的所有字符不进行拷贝，等于过滤了注释。
     * 一直过滤直到遇到注释的右端字符后才重新开始拷贝数据。
     */

    for(i = 0; i < nums; i++)
    {
        if(commented_code[i]=='/' && commented_code[i+1]=='*')
        {
            signal = true;
        }
        else if(commented_code[i]=='/' && commented_code[i-1]=='*')
        {
            signal = false;
        }
        else if(signal == false)
        {
            no_commented_code[j] = commented_code[i];
            j++;
        }
    }
}
