#include<stdio.h>
int main()
{
    int space = 0, tab = 0, lf = 0;
    int c;

    while((c=getchar()) != EOF)
    {
        if(c == ' ')
        {
            ++space;
        }
        else if(c == '\t')
        {
            ++tab;
        }
        else if(c == '\n')
        {
            ++lf;
        }
    }
    printf("\n输入的空格：%d, 制表符：%d, 换行符：%d", space, tab, lf);
    return 0;
}