#include<stdio.h>
#include<stdlib.h>
#define MAXLINE 1000    /* 允许的输入行的最大长度 */

int getline(char line[], int maxline);
void copy(char to[], char from[]);

/* 打印最长的输入行 */
int main()
{
    int len;                /* 当前行长度 */
    char line[MAXLINE];     /* 当前的输入行 */
    char anyline[MAXLINE];  /* 保存任意行 */

    while((len = getline(line, MAXLINE)) > 0)   /* 每次输入除了EOF则至少有一个字符被传入 */
    {
        copy(anyline,line);
        printf("%s", anyline);
    }
    return 0;
}

/* getline函数：将一行读入s中并返回其长度 */
int getline(char s[], int lim)
{
    int c, i;

    for(i=0; i<lim-1 && (c=getchar())!=EOF && c != '\n'; ++i)
        s[i] = c;       /* 循环读取每一个字符，遇到换行符或文件结尾跳出 */
    if (c == '\n')      /* 若是换行符则在字符数组末尾加上换行符 */
    {
        s[i] = c;
        ++i;
    }
    s[i] = '\0';        /* 换行符后再添加字符串结尾符 */
    return i;
}

/* copy函数：将from复制到to；这里假定to足够大，若from大于to会直接报错 */
void copy(char to[], char from[])
{
    int i;
    
    i = 0;
    while((to[i] = from[i]) != '\0')
        ++i;
}
