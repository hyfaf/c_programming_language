#include<stdio.h>
int main()
{
    int c;
    int sign = 0;  /* 设置一个标志位，标识空格 */

    while((c=getchar())!=EOF)
    {
        if(c==' ')
        {
            sign = 1;
            continue;
        }
        else if(c != ' ' && sign == 1)
        {
            putchar(' ');
            sign = 0;
        }

        putchar(c);
    }
    return 0;
}