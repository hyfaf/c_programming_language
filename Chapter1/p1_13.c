#include<stdio.h>
#define MAX_LENGTH 45

int main()
{
    int word_length[MAX_LENGTH] = {0};
    char c;
    int length = 0;

    while((c=getchar())!=EOF)
    {
        if(c == ' ' || c == '\t' || c == '\n' || c == EOF)
        {
            word_length[length]++;
            length = 0;
            continue;
        }
        length++;
    }

    // 打印一个水平的直方图
    printf("\n------单词长度直方图------\n");
    for (int i = 1; i < MAX_LENGTH; i++)
    {
        if(word_length[i] == 0)
            continue;

        printf("%2d words:", i);
        for (int j = 0; j < word_length[i]; j++)
        {
            printf("-");
        }
        printf("  %d", word_length[i]);
        printf("\n");
    }

    // 垂直直方图打印略有难度
    // 后续再写

    return 0;
}