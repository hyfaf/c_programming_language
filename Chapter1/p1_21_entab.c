#include<stdio.h>

void replace_space(char c);
int main()
{
    int c;

    while((c=getchar())!=EOF)
    {
        if(c == '\t')
            replace_space(c);
        else
            putchar(c);
    }

    return 0;
}

/* 小于三个空格转换成一个空格，4个空格转换成一个tab */
void replace_space(char c)
{
    int n = 4;      
    int i;

    for (i=0; i<4; i++)
        putchar(' ');
}