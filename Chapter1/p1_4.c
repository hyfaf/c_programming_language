/* 打印一个摄氏度转华氏度的转换表 */
#include<stdio.h>
int main()
{
    float fahr, celsius;
    int lower, upper, step;

    lower = 0;
    upper = 300;
    step = 20;

    celsius = lower;
    printf("摄氏度-华氏度转换表\n");
    while(celsius <= upper)
    {
        // celsius = (5.0/9.0) * (fahr-32.0);
        fahr = celsius / (5.0/9.0) + 32.0;
        printf("%3.0f %6.1f\n", celsius, fahr);
        celsius = celsius + step;
    }

    return 0;
}